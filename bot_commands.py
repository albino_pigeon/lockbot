import re
import logging

from chat_functions import send_text_to_room
from slave import Slave

logger = logging.getLogger(__name__)


class Command(object):
    def __init__(self, client, store, config, command, room, event, user_id_regex, slave_dict):
        """A command made by a user

        Args:
            client (nio.AsyncClient): The client to communicate to matrix with

            store (Storage): Bot storage

            config (Config): Bot configuration parameters

            command (str): The command and arguments

            room (nio.rooms.MatrixRoom): The room the command was sent in

            event (nio.events.room_events.RoomMessageText): The event describing the command

            user_id_regex (Pattern[AnyStr]): A compiled regex for finding user ids

            slave_dict (dict[str,Slave]): A dictionary relating all known user IDs and their related Slave objects
        """
        self.client = client
        self.store = store
        self.config = config
        self.command = command
        self.room = room
        self.event = event
        self.args = self.command.split()[1:]

        # Retrieve dictionary of user_id: slave
        self.slave_dict = slave_dict

        self.user_id_regex = user_id_regex

    async def process(self):
        """Process the command"""
        # Slave states who they want their keyholder to be
        # Slave can revoke keyholder status from themselves
        # Keyholder can use commands on a slave
        # Slave can't use any commands except for add/remove keyholder
        if self.command.startswith("new_slave"):
            await self._new_slave()
        elif self.command.startswith("lock"):
            await self._lock()
        elif self.command.startswith("unlock"):
            await self._unlock()
        elif self.command.startswith("status"):
            await self._status()
        elif self.command.startswith("add_orgasm"):
            await self._add_orgasm()
        elif self.command.startswith("set_unlock_date"):
            await self._set_unlock_date()
        elif self.command.startswith("set_max_orgasms"):
            await self._set_max_orgasms()
        elif self.command.startswith("get_max_orgasms"):
            await self._get_max_orgasms()
        elif self.command.startswith("help"):
            await self._show_help()
        else:
            await self._unknown_command()

    async def _new_slave(self):
        """Create a new slave"""
        if len(self.args) < 1:
            await self._respond("Usage: `new_slave user_id`")
            return

        # Check for valid user id
        # Get display name of the user
        slave_user_id = self.args[0]
        if not self._check_user_id_syntax(slave_user_id):
            await self._respond(f"Not a valid user id: {slave_user_id}.")
            return

        # Check that this user doesn't already exist
        if slave_user_id in self.slave_dict:
            keyholders = self.slave_dict[slave_user_id].keyholders
            await self._respond(f"This slave is already owned by {', '.join(keyholders)}")
            return

        if slave_user_id == self.event.sender:
            await self._respond("You cannot own yourself.")
            return

        slave = Slave(self.store, slave_user_id)

        await slave.add_keyholder(self.event.sender)
        await slave.save()

        self.slave_dict[slave_user_id] = slave
        await self._respond(f"Created new slave {slave_user_id}, keyholder: {self.event.sender}.")

    async def _unlock(self):
        """Unlock a slave"""
        if len(self.args) < 1:
            await self._respond("Usage: `unlock user_id`")
            return

        slave = self.slave_dict.get(self.args[0])
        if not slave:
            await self._respond("Unknown slave.")
            return

        if not await slave.is_keyholder(self.event.sender):
            await self._respond(f"You do not own {slave.user_id}.")
            return

        if not slave.locked:
            await self._respond(f"{slave.user_id} is already unlocked.")
            return

        await slave.unlock()
        await self._respond(f"{slave.user_id} has been unlocked.")

    async def _lock(self):
        """Lock a slave up"""
        if len(self.args) < 1:
            await self._respond("Usage: `lock user_id`")
            return

        slave = self.slave_dict.get(self.args[0])
        if not slave:
            await self._respond("Unknown slave.")
            return

        if not await slave.is_keyholder(self.event.sender):
            await self._respond(f"You do not own {slave.user_id}.")
            return

        if slave.locked:
            await self._respond(f"{slave.user_id} is already locked up.")
            return

        await slave.lock()
        await self._respond(f"{slave.user_id} has been locked up.")

    async def _add_orgasm(self):
        """Add a new orgasm to the slave's current year counter"""
        if len(self.args) < 1:
            await self._respond("Usage: `add_orgasm user_id`")
            return

        # Get slave from user id
        slave = self.slave_dict.get(self.args[0])
        if not slave:
            await self._respond("Unknown slave.")
            return

        if not await slave.is_keyholder(self.event.sender):
            await self._respond(f"You do not own {slave.user_id}.")
            return

        await slave.modify_orgasms(1)
        text = (
            f"Orgasm added. Total for this year: {await slave.get_orgasm_count()}."
            f" Allowed for this year: {await slave.get_max_orgasm_count_for_year()}."
        )
        await self._respond(text)

    async def _remove_orgasm(self):
        """Removes an orgasm from the slave's current year counter"""
        if len(self.args) < 1:
            await self._respond("Usage: `remove_orgasm user_id`")
            return

        # Get slave from user id
        slave = self.slave_dict.get(self.args[0])
        if not slave:
            await self._respond("Unknown slave.")
            return

        if not await slave.is_keyholder(self.event.sender):
            await self._respond(f"You do not own {slave.user_id}.")
            return

        await slave.modify_orgasms(-1)
        text = (
            f"Orgasm removed. Total for this year: {await slave.get_orgasm_count()}."
            f" Allowed for this year: {await slave.get_max_orgasm_count_for_year()}."
        )
        await self._respond(text)

    async def _set_unlock_date(self):
        """Define when a slave can be unlocked"""
        # TODO: Set a timer
        if len(self.args) < 1:
            await self._respond("Usage: `set_unlock_date user_id [YYYY-MM-DD]`")
            return

        slave_user_id = self.args[0]
        unlock_date = None if len(self.args) == 1 else self.args[1]

        # Get slave from user id
        slave = self.slave_dict.get(slave_user_id)
        if not slave:
            await self._respond("Unknown slave.")
            return

        if not await slave.is_keyholder(self.event.sender):
            await self._respond(f"You do not own {slave.user_id}.")
            return

        days_until_release = await slave.set_unlock_date(unlock_date)
        if unlock_date:
            text = f"Unlock date set for {unlock_date}. Days until release: {days_until_release}."
        else:
            text = "Unlock date removed."
        await self._respond(text)

    async def _status(self):
        """Get a slave's status"""
        if len(self.args) < 1:
            await self._respond("Usage: `status user_id`")
            return

        # Get slave from user id
        slave = self.slave_dict.get(self.args[0])
        if not slave:
            await self._respond("Unknown slave.")
            return

        # Get a nicely-readable name for a user
        slave_display_name = self.room.user_name(slave.user_id)
        if not slave_display_name:
            # Fallback to user ID
            slave_display_name = slave.user_id

        locked_date = await slave.get_locked_date()
        text = f"""
## Status for slave {slave_display_name}:

* Locked: {slave.locked}

* Unlock date: {await slave.get_unlock_date()}

* Used orgasms this year: {await slave.get_orgasm_count()}

* Allowed orgasms this year: {await slave.get_max_orgasm_count_for_year()}

{slave_display_name} has been locked for {await slave.get_days_since_locked()}
days{f" (since {locked_date})" if locked_date else ""}.
        """
        await self._respond(text)

    async def _set_max_orgasms(self):
        """Set maximum allowed orgasms for a year"""
        if len(self.args) < 3:
            await self._respond("Usage: `set_year_orgasms user_id year orgasm_count`")
            return

        slave_user_id, year, orgasms = self.args
        year = int(year)
        orgasms = int(orgasms)

        if orgasms < 0:
            await self._respond("Number of orgasms must be a positive number.")
            return

        # Get slave from user id
        slave = self.slave_dict.get(self.args[0])
        if not slave:
            await self._respond("Unknown slave.")
            return

        if not await slave.is_keyholder(self.event.sender):
            await self._respond(f"You do not own {slave.user_id}.")
            return

        # Set orgasm count for a year
        try:
            await slave.set_max_orgasm_count_for_year(year, orgasms)
        except Exception:
            error = f"Error setting max orgasm count for {slave.user_id}"
            logger.exception(error)
            await self._respond(error)
            return

        await self._respond(f"Max orgasm count for {year} set to {orgasms}.")

    async def _get_max_orgasms(self):
        """Get year/used orgasms/max orgasm count for a slave"""
        if len(self.args) < 1:
            await self._respond("Usage: `get_year_orgasms user_id`")
            return

        # Get slave from user id
        slave = self.slave_dict.get(self.args[0])
        if not slave:
            await self._respond("Unknown slave.")
            return

        text = ""
        for year, orgasms in slave.years.items():
            used_orgasms, max_orgasms = orgasms

            # Nicely format max orgasm count if undefined
            max_orgasms = max_orgasms if max_orgasms else "Unlimited"
            text += f"* {year}: Used orgasms: {used_orgasms} / {max_orgasms}\n\n"

        await self._respond(text)

    async def _show_help(self):
        """Show the help text"""
        if not self.args:
            text = (
                "Hello, I'm lockbot! I'm here to make keyholders and their sub's lives easier. "
                "Use `help commands` to view available commands."
            )
            await send_text_to_room(self.client, self.room.room_id, text)
            return

        topic = self.args[0]
        if topic == "rules":
            text = "These are the rules!"
        elif topic == "commands":
            text = """
# Available commands:

**Keyholders only:**

<pre>
lock user_id:
  Lock a slave up.

unlock user_id:
  Let a slave out.

add_orgasm user_id:
  Add one to the slave's orgasm count.

remove_orgasm user_id:
  Remove one from the slave's orgasm count.

set_max_orgasms user_id year count:
  Set the maximum amount of orgasms allowed for a given year.

set_unlock_date user_id [YYYY-MM-DD]:
  Modify the next date that the slave is allowed out. Leave out the date for infinite.
</pre>

**Everyone**:

<pre>
new_slave user_id:
  Own a new slave.

status user_id:
  Display information about a slave.

get_max_orgasms user_id:
  Return the maximum allowed orgasms per year for a slave.

help:
  Show this help text.
</pre>
            """
        else:
            text = "Unknown help topic!"
        await send_text_to_room(self.client, self.room.room_id, text)

    def _check_user_id_syntax(self, user_id):
        """Check that a given str matches the syntax of a user ID

        Args:
            user_id (str): The user id str to check

        Returns:
            bool: Whether the str matches or not
        """
        return bool(self.user_id_regex.match(user_id))

    async def _unknown_command(self):
        await send_text_to_room(
            self.client,
            self.room.room_id,
            f"Unknown command '{self.command}'. Try the 'help' command for more information.",
        )

    async def _respond(self, text):
        """Send text to the room of the current command

        Args:
            text (str): The text to send to the room
        """
        await send_text_to_room(self.client, self.room.room_id, text)
