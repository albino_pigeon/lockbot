from datetime import datetime
import logging

logger = logging.getLogger(__name__)


class Slave(object):
    def __init__(self, store, user_id, lock_date=None, unlock_date=None, locked=False):
        """A locked slave

        Args:
            store (Storage): Bot storage

            user_id (str): The slave's user_id

            lock_date (datetime|None): When the slave was last locked

            unlock_date (datetime|None): When the slave will next be unlocked

            locked (bool): Whether the slave is currently locked up
        """
        self.store = store
        self.user_id = user_id
        self.keyholders = []
        self.locked = locked
        self.lock_date = lock_date
        self.unlock_date = unlock_date

        # Dictionary of year: [current_orgasms, max_orgasms]
        self.years = {}

    @staticmethod
    def setup_db(cursor):
        """Setup the sync token db table

        Args:
            cursor (sqlite3.Cursor): Database cursor
        """
        # Sync token table
        cursor.execute("CREATE TABLE slave ("
                       " user_id TEXT PRIMARY KEY,"
                       " locked BOOLEAN NOT NULL,"
                       " lock_date INTEGER,"
                       " unlock_date INTEGER"
                       ")")
        cursor.execute("CREATE UNIQUE INDEX slave_user_id ON slave (user_id)")

        cursor.execute("CREATE TABLE slave_keyholder ("
                       " slave_user_id TEXT NOT NULL,"
                       " keyholder_user_id TEXT NOT NULL"
                       ")")
        cursor.execute("CREATE UNIQUE INDEX slave_keyholder_slave_user_id"
                       " ON slave_keyholder (slave_user_id, keyholder_user_id)")

        cursor.execute("CREATE TABLE slave_year_orgasms ("
                       " slave_user_id TEXT NOT NULL,"
                       " year INTEGER NOT NULL,"
                       " used_orgasms INTEGER NOT NULL,"
                       " max_orgasms INTEGER"
                       ")")
        cursor.execute("CREATE UNIQUE INDEX slave_year_orgasms_slave_id ON slave_year_orgasms (slave_user_id, year)")

    @staticmethod
    def retrieve_slaves(store):
        """Returns all known slaves from the database

        Args:
            store (Storage): Bot storage

        Returns:
            Dict[str:Slave]: A dictionary of user_id: Slave mappings
        """
        # Search the database for this user_id
        store.cursor.execute("SELECT user_id, locked, lock_date, unlock_date"
                             " FROM slave")
        slaves = store.cursor.fetchall()

        # Create initial slave objects
        slave_dict = {}

        for slave in slaves:
            user_id, locked, lock_date, unlock_date = slave

            if unlock_date:
                # Convert from timestamp to datetime object
                unlock_date = datetime.fromtimestamp(unlock_date)

            if lock_date:
                # Convert from timestamp to datetime object
                lock_date = datetime.fromtimestamp(lock_date)

            new_slave = Slave(
                store,
                user_id,
                lock_date=lock_date,
                unlock_date=unlock_date,
                locked=bool(locked),
            )
            slave_dict[user_id] = new_slave

        # Retrieve known keyholders
        store.cursor.execute("SELECT slave_user_id, keyholder_user_id "
                             " FROM slave_keyholder")
        slave_keyholder_mappings = store.cursor.fetchall()

        for slave_user_id, keyholder_user_id in slave_keyholder_mappings:
            slave_dict[slave_user_id].keyholders.append(keyholder_user_id)

        # Retrieve year orgasm information
        store.cursor.execute("SELECT slave_user_id, year, used_orgasms, max_orgasms "
                             " FROM slave_year_orgasms")
        slave_year_mappings = store.cursor.fetchall()

        for slave_user_id, year, used_orgasms, max_orgasms in slave_year_mappings:
            slave_dict[slave_user_id].years[year] = [used_orgasms, max_orgasms]

        return slave_dict

    async def add_keyholder(self, keyholder_user_id):
        """Adds a keyholder to this slave

        Args:
            keyholder_user_id (str): The keyholder's user ID
        """
        self.keyholders.append(keyholder_user_id)

        # Add the entry to the database
        self.store.cursor.execute("INSERT INTO slave_keyholder (slave_user_id, keyholder_user_id)"
                                  " VALUES (?, ?)",
                                  (self.user_id, keyholder_user_id))
        self.store.conn.commit()

    async def remove_keyholder(self, keyholder_user_id):
        """Removes a keyholder from this slave

        Args:
            keyholder_user_id (str): The keyholder's user ID
        """
        self.keyholders.remove(keyholder_user_id)

        # Remove the entry from the database
        self.store.cursor.execute("DELETE FROM slave_keyholder"
                                  " WHERE slave_user_id = ?,"
                                  " AND keyholder_user_id = ?",
                                  (self.user_id, keyholder_user_id))
        self.store.conn.commit()

    async def is_keyholder(self, user_id):
        """Check whether this user_id is a known keyholder of this slave.

        Args:
            user_id (str): A user ID

        Returns:
            bool: Whether this user_id is a keyholder of this slave
        """
        return user_id in self.keyholders

    async def lock(self):
        """Lock a slave up"""
        now_timestamp = datetime.now().timestamp()
        self.locked = True
        self.store.cursor.execute("UPDATE slave SET"
                                  " locked = 1,"
                                  " lock_date = ?"
                                  " WHERE user_id = ?",
                                  (now_timestamp, self.user_id))
        self.store.conn.commit()

    async def unlock(self):
        """Unlock a slave"""
        self.locked = False
        self.store.cursor.execute("UPDATE slave SET"
                                  " lock_date = NULL"
                                  " locked = 0"
                                  " WHERE user_id = ?",
                                  (self.user_id,))
        self.store.conn.commit()

    async def set_unlock_date(self, date):
        """Set a time/date that this slave's lock will open

        Args:
            date (str|None): Date that the slave will be unlocked.
                Must be in format YYYY-MM-DD. If None, date is removed.

        Returns:
            int|None: Days until the set unlock date. None if no date was specified
        """
        if date:
            # Parse the given date into a datetime and timestamp
            dt = datetime.strptime(date, "%Y-%m-%d")
            timestamp = dt.timestamp()
        else:
            # Remove unlock date
            dt = None
            timestamp = None

        # Save timestamp/datetime object
        self.unlock_date = dt
        self.store.cursor.execute("UPDATE slave SET"
                                  " unlock_date = ?"
                                  " WHERE user_id = ?",
                                  (timestamp, self.user_id))
        self.store.conn.commit()

        if date:
            # Calculate days until set unlock date
            now = datetime.now()
            return (dt-now).days
        else:
            return None

    async def get_days_since_locked(self):
        """Get the days since this slave was locked up

        Returns:
            int: The days since the slave was locked. 0 if they are currently unlocked
        """
        if not self.locked or not self.lock_date:
            return 0
        now = datetime.now()
        return (now - self.lock_date).days

    async def get_locked_date(self):
        """Get the date that this slave was locked up

        Returns:
            str|None: Get a human readable date of when this slave was locked up
        """
        if not self.lock_date:
            return None

        return self.lock_date.strftime("%Y-%m-%d")

    async def get_unlock_date(self):
        """Get a human-readable unlock date"""
        if not self.unlock_date:
            return "Not defined"

        return self.unlock_date.strftime("%Y-%m-%d")

    async def get_orgasm_count(self, year=None):
        """Get this slave's orgasm count for this year/all time

        Args:
            year (int|None): Year number to get orgasms for. None for current year

        Returns:
            int: The orgasms in that year. Defaults to 0 if unknown
        """
        if not year:
            # Get current year
            now = datetime.now()
            year = now.year

        year_tuple = self.years.get(year)
        if not year_tuple:
            return 0

        return year_tuple[0]

    async def get_max_orgasm_count_for_year(self, year=None):
        """Get maximum allowed orgasms for a given year

        Args:
            year (int|None): The year to get maximum allowed orgasms for. None for the current year

        Returns:
            int|str: The number of allowed orgasms in a given year. "Not defined" if undefined
        """
        if not year:
            # Get current year
            now = datetime.now()
            year = now.year

        year_tuple = self.years.get(year)
        if not year_tuple:
            return "Not defined"

        max_orgasms = year_tuple[1]

        return max_orgasms if max_orgasms else "Not defined"

    async def modify_orgasms(self, orgasm_count):
        """Modifies the orgasm count for the current year

        Returns:
            int: The relative number to modify the slave's orgasm count for this year by
        """
        # Get current year
        now = datetime.now()
        curr_year = now.year

        # See if an entry for this year exists already
        if curr_year not in self.years:
            used_orgasms = orgasm_count
            max_orgasms = None  # undefined
            self.years[curr_year] = [used_orgasms, max_orgasms]
        else:
            used_orgasms, max_orgasms = self.years[now.year]
            used_orgasms += orgasm_count
            self.years[now.year] = [used_orgasms, max_orgasms]

        # Save the current orgasm count for this year
        self.store.cursor.execute("DELETE FROM slave_year_orgasms"
                                  " WHERE slave_user_id = ?"
                                  " AND year = ?",
                                  (self.user_id, curr_year))
        self.store.cursor.execute("INSERT INTO slave_year_orgasms (slave_user_id, year, used_orgasms, max_orgasms)"
                                  " VALUES (?, ?, ?, ?)",
                                  (self.user_id, curr_year, used_orgasms, max_orgasms))
        self.store.conn.commit()

        return used_orgasms

    async def set_max_orgasm_count_for_year(self, year, max_orgasms):
        """Set maximum amount of orgasms for this slave for a given year

        Args:
            year (int): The year to set maximum allowed orgasms for

            max_orgasms (int): Allowed number of orgasms
        """
        # Get orgasm count for this year
        year_tuple = self.years.get(year)
        if year_tuple:
            used_orgasms = year_tuple[0]
        else:
            used_orgasms = 0

        # Save the current orgasm count for this year
        self.years[year] = [0, max_orgasms]
        self.store.cursor.execute("DELETE FROM slave_year_orgasms"
                                  " WHERE slave_user_id = ?"
                                  " AND year = ?",
                                  (self.user_id, year))
        self.store.cursor.execute("INSERT INTO slave_year_orgasms (slave_user_id, year, used_orgasms, max_orgasms)"
                                  " VALUES (?, ?, ?, ?)",
                                  (self.user_id, year, used_orgasms, max_orgasms))
        self.store.conn.commit()

    async def save(self):
        """Save new slave to the database"""
        self.store.cursor.execute("INSERT INTO slave (user_id, locked)"
                                  " VALUES (?, 0)",
                                  (self.user_id,))
        self.store.conn.commit()
