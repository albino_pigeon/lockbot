import logging
import re

from bot_commands import Command
from nio import (
    JoinError,
)
from message_responses import Message
from slave import Slave

logger = logging.getLogger(__name__)


class Callbacks(object):

    def __init__(self, client, store, config):
        """
        Args:
            client (nio.AsyncClient): nio client used to interact with matrix

            store (Storage): Bot storage

            config (Config): Bot configuration parameters
        """
        self.client = client
        self.store = store
        self.config = config
        self.command_prefix = config.command_prefix

        self.pill_regex = re.compile(r'<a href="https://matrix\.to/#/(@.*:.+)">.*</a>')
        self.user_id_regex = re.compile(r"@.*:.+")

        # Since user's can make pills for themselves, translate any mention of 'me' into
        # that user's user id
        # TODO: use one regex
        self.self_mention_regex = re.compile(r' me ')
        self.self_mention_regex_end = re.compile(r' me$')

        # Retrieve dictionary of user_id: slave
        self.slave_dict = Slave.retrieve_slaves(store)

    async def message(self, room, event):
        """Callback for when a message event is received

        Args:
            room (nio.rooms.MatrixRoom): The room the event came from

            event (nio.events.room_events.RoomMessageText): The event defining the message

        """
        # Extract the formatted message text
        msg = event.formatted_body or event.body

        # Ignore messages from ourselves
        if event.sender == self.client.user:
            return

        logger.debug(
            f"Bot message received for room '{room.display_name}' | "
            f"{room.user_name(event.sender)}: {msg}"
        )

        # Process as message if in a public room without command prefix
        has_command_prefix = msg.startswith(self.command_prefix)
        if not has_command_prefix and not room.is_group:
            # General message listener
            message = Message(self.client, self.store, self.config, msg, room, event)
            await message.process()
            return

        # Otherwise if this is in a 1-1 with the bot or features a command prefix,
        # treat it as a command
        if has_command_prefix:
            # Remove the command prefix
            msg = msg[len(self.command_prefix):]

            # Convert any user pills to user ids
            msg = self.pill_regex.sub(r'\g<1>', msg)

            # Convert any mention of `me` into that user's id
            msg = self.self_mention_regex.sub(f' {event.sender} ', msg)
            msg = self.self_mention_regex_end.sub(f' {event.sender}', msg)

        command = Command(self.client, self.store, self.config, msg, room, event, self.user_id_regex, self.slave_dict)
        await command.process()

    async def invite(self, room, event):
        """Callback for when an invite is received. Join the room specified in the invite"""
        logger.debug(f"Got invite to {room.room_id} from {event.sender}.")

        # Attempt to join 3 times before giving up
        for attempt in range(3):
            result = await self.client.join(room.room_id)
            if type(result) == JoinError:
                logger.error(
                    f"Error joining room {room.room_id} (attempt %d): %s",
                    attempt, result.message,
                )
            else:
                logger.info(f"Joined {room.room_id}")
                break

