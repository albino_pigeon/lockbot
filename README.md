# Lockbot

A bot for keyholders to keep track of their subs and their progress. Made with
[nio-template](https://github.com/anoadragon453/nio-template).

## Setup

Copy `sample.config.yaml` to `config.yaml` and edit the `matrix:` section at
the least.

Start the bot with `python ./main.py`.

## Usage

Invite the bot's user to a room with your sub. Use the `!help` command to see
what you can do :)
